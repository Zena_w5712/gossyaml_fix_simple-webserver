#!/bin/bash

# Install goss for testing
# See https://github.com/aelsabbahy/goss/releases for release versions
sudo curl -L https://github.com/aelsabbahy/goss/releases/download/v0.3.2/goss-linux-amd64 -o /usr/local/bin/goss
sudo chmod +rx /usr/local/bin/goss

sudo mkdir /opt/healthz
sudo cp /tmp/files/goss.yaml /opt/healthz

cd /opt/healthz

# Run tests
goss validate