# Packer Simple Webserver
## How does it work?

### AWS ECS (Fargate)
This Grafana application is hosted on AWS Fargate. AWS Fargate is:
> AWS Fargate is a compute engine for Amazon ECS that allows you to run containers without having to manage servers or clusters. With AWS Fargate, you no longer have to provision, configure, and scale clusters of virtual machines to run containers. This removes the need to choose server types, decide when to scale your clusters, or optimize cluster packing.

It is clear to see the benefits of using AWS ECS, however crucially because we are using AWS Fargate we are not managing EC2 instances.

> Amazon ECS has two modes: Fargate launch type and EC2 launch type. With Fargate launch type, all you have to do is package your application in containers, specify the CPU and memory requirements, define networking and IAM policies, and launch the application. EC2 launch type allows you to have server-level, more granular control over the infrastructure that runs your container applications.

To read more please visit: [https://aws.amazon.com/fargate/](https://aws.amazon.com/fargate/)

### Grafana
As discussed above we are using AWS Fargate and therefore we need a container to host. This container is provided by one of our Docker projects: [autobots-grafana](https://bitbucket.org/geoscienceaustralia/autobots-grafana/src/master/)
This project uses the official Grafana Docker container as a base image and adds a few other necessary changes to make it easier to deploy using AWS Fargate (you can view the Docker file at: [https://bitbucket.org/geoscienceaustralia/autobots-grafana/src/master/Dockerfile](https://bitbucket.org/geoscienceaustralia/autobots-grafana/src/master/Dockerfile)).

The most important part are the commands which install and run **Chamber**. Chamber is responsible for extracting AWS Systems Manager Paremeter Store secrets, and storing them in the container as environment variables. This means that every time this container is launched we can store information stored in AWS SSM Parameter Store (passwords, database enpoints etc.) as environment variables. This is necessary because Grafana is configured using a large set of specific environment variables. More information on configuring Grafana can be found here: [https://grafana.com/docs/installation/configuration/](https://grafana.com/docs/installation/configuration/). The environment variables used for this project have been listed below (note: the variables names begin with `/grafana/` so that Chamber can differentiate between them and any other variable stored in AWS SSM Parameter Store):
`
AWS_REGION
GF_AUTH_GENERIC_OAUTH_ALLOWED_DOMAINS
GF_AUTH_GENERIC_OAUTH_ALLOW_SIGN_UP
GF_AUTH_GENERIC_OAUTH_ENABLED
GF_AUTH_GENERIC_OAUTH_SCOPES
GF_AUTH_GENERIC_OAUTH_NAME
GF_AWS_PROFILES
GF_AWS_default_REGION
GF_LOG_LEVEL
GF_LOG_MODE
GF_USERS_AUTO_ASSIGN_ORG_ROLE
GF_INSTALL_PLUGINS
GF_AUTH_GENERIC_OAUTH_AUTH_URL
GF_AUTH_GENERIC_OAUTH_TOKEN_URL
GF_AUTH_GENERIC_OAUTH_CLIENT_ID
GF_AUTH_GENERIC_OAUTH_CLIENT_SECRET
GF_SECURITY_ADMIN_PASSWORD
GF_DATABASE_TYPE
GF_DATABASE_USER
GF_DATABASE_PASSWORD
GF_DATABASE_NAME
`

### Terraform
testing

## Packages
1. Apache2
1. Ruby (for running tests)
1. ufw (firewall)
1. fail2ban (blocks IPs committing suspicious activity)

## Usage
1. Download and install Packer from [packer.io](http://packer.io)
2. `packer build -var "environment=dev" build.json`
3. Deploy a new EC2 instance using the AMI that was created in step 2.
4. Head to the public DNS record for the new instance to see your 'Hello, world!'

## Advanced usage
1. add home ips to fail2ban whitelist:
    1. `export HOME_IP_HTML=yourip`
2. `packer build -var "environment=dev" build.json`

<!-- Breaks this into two lists -->

1. add html and ssh ips to fail2ban whitelist
    1. `export HOME_IP_HTML=yourip`
    2. `export HOME_IP_SSH=yoursship`
2. `packer build -var "environment=dev" build.json`

## Customisations from default apache

### Network
* Configure a software firewall
* Secure shared memory
* IP Spoofing protection
* Ignore ICMP broadcast requests
* Disable source packet routing
* Ignore send redirects
* Block SYN Attacks
* Log Martians
* Ignore ICMP Redirects
* Ignore Directed pings

### Apache
* Hide server signatures and headers
* Turn TraceEnable Off

### Fail2Ban
* Set Ignore IPs to GA
* Monitor apache and sshd logs for attacks, ban IPs for 10 minutes

## Contributing
* Tests are defined in the tests folder and use [serverspec](http://serverspec.org/)
* If you have added any software please ensure it is reflected in the tests

## ROADMAP
### Add HTTPS Support
* Generate self signed cert (to sit behind elb)
* configure apache to work with ssl

### Improve Security logging
* Install PSAD
* Install RKHunter and CHKRootKit
* Install NMAP
* Setup LogWatch
* Setup Tiger and Tripwire
* Run all of these tools nightly
* Install logstash / fluentd
* send logs to monitoring server by default
* Add prometheus support
